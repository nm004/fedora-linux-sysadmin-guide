
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-rpm-ostree]]
= rpm-ostree

[application]*rpm-ostree* is a hybrid image/package system for {OSORG}.  One of
the most important things to understand is that available RPMs come from the same
`/etc/yum.repos.d` sources.

For more information, see the link:https://rpm-ostree.readthedocs.io/en/latest/[upstream documentation].
