:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-RPM]]
= RPM
indexterm:[RPM Package Manager,RPM]indexterm:[RPM]indexterm:[packages,RPM]
The _RPM Package Manager_ ([application]*RPM*) is an open packaging system that runs on {MAJOROS} as well as other Linux and UNIX systems. Red{nbsp}Hat and the Fedora Project encourage other vendors to use [application]*RPM* for their own products. [application]*RPM* is distributed under the terms of the _GPL_ (_GNU General Public License_).

The [application]*RPM Package Manager* only works with packages built in the *RPM format*. [application]*RPM* itself is provided as the pre-installed [package]*rpm* package. For the end user, [application]*RPM* makes system updates easy. Installing, uninstalling, and upgrading [application]*RPM* packages can be accomplished with short commands. [application]*RPM* maintains a database of installed packages and their files, so you can make queries and verify installed files on your system. There are several applications, such as [application]*DNF* or [application]*PackageKit*, that can make working with packages in the [application]*RPM* format even easier.

[[warning-Use_DNF_Instead_of_RPM_Whenever_Possible]]
.Use DNF Instead of RPM Whenever Possible
[WARNING]
====
indexterm:[packages,DNF instead of RPM]
For most package-management tasks, the [application]*DNF* package manager offers equal and often greater capabilities and utility than [application]*RPM*. [application]*DNF* also performs and tracks complicated system-dependency resolutions. [application]*DNF* maintains the system integrity and forces a system integrity check if packages are installed or removed using another application, such as [application]*RPM*, instead of [application]*DNF*. For these reasons, it is highly recommended that you use [application]*DNF* instead of [application]*RPM* whenever possible to perform package-management tasks. See xref:package-management/DNF.adoc#ch-DNF[DNF].

If you prefer a graphical interface, you can use the [application]*PackageKit* GUI application, which uses [application]*DNF* as its back end, to manage your system's packages.

====

During upgrades, [application]*RPM* handles configuration files carefully, so that you never lose your customizations &mdash; something that you cannot accomplish with regular `.tar.gz` files.
indexterm:[packages,RPM,source and binary packages]
For the developer, [application]*RPM* enables software source code to be packaged into source and binary packages for end users. This process is quite simple and is driven from a single file and optional patches that you create. This clear delineation between pristine sources and your patches along with build instructions eases the maintenance of the package as new versions of the software are released.

[[note-Root_Permissions]]
.Note
[NOTE]
====

Because [application]*RPM* can make changes to the system itself, performing operations like installing, upgrading, downgrading, and uninstalling binary packages system-wide requires `root` privileges in most cases.

====

[[s1-rpm-design]]
== RPM Design Goals
indexterm:[RPM,design goals]
To understand how to use [application]*RPM*, it is helpful to understand the design goals of [application]*RPM*:

indexterm:[RPM,design goals,upgradability]          Upgradability::  With [application]*RPM*, you can upgrade individual components of your system without a complete reinstallation. When you get a new release of an operating system based on [application]*RPM*, such as {MAJOROS}, you do not need to reinstall a fresh copy of the operating system on your machine (as you might need to with operating systems based on other packaging systems). [application]*RPM* allows for intelligent, fully-automated, in-place upgrades of your system. In addition, configuration files in packages are preserved across upgrades, so you do not lose your customizations. There are no special upgrade files needed to upgrade a package because the same [application]*RPM* file is used to both install and upgrade the package on the system.

indexterm:[RPM,design goals,powerful querying]          Powerful Querying::  [application]*RPM* is designed to provide powerful querying options. You can perform searches on your copy of the database for packages or even just certain files. You can also easily find out what package a file belongs to and where the package came from. The files an [application]*RPM* package contains are in a compressed archive, with a custom binary header containing useful information about the package and its contents, allowing you to query individual packages quickly and easily.

indexterm:[RPM,design goals,system verification]          System Verification::  Another powerful [application]*RPM* feature is the ability to verify packages. It allows you to verify that the files installed on the system are the same as the ones supplied by a given package. If an inconsistency is detected, [application]*RPM* notifies you, and you can reinstall the package if necessary. Any configuration files that you modified are preserved during reinstallation.

indexterm:[packages,RPM,pristine sources]          Pristine Sources::  A crucial design goal was to allow the use of *pristine* software sources, as distributed by the original authors of the software. With [application]*RPM*, you have the pristine sources along with any patches that were used, plus complete build instructions. This is an important advantage for several reasons. For instance, if a new version of a program is released, you do not necessarily have to start from scratch to get it to compile. You can look at the patch to see what you *might* need to do. All the compiled-in defaults, and all of the changes that were made to get the software to build properly, are easily visible using this technique.

The goal of keeping sources pristine may seem important only for developers, but it results in higher quality software for end users.

[[s1-rpm-using]]
== Using RPM

[application]*RPM* has five basic modes of operationindexterm:[RPM,basic modes] (not counting package building): installing, uninstalling, upgrading, querying, and verifying. This section contains an overview of each mode. For complete details and options, try [command]#rpm --help# or see *rpm*(8). Also, see xref:RPM.adoc#s1-rpm-additional-resources[Additional Resources] for more information on [application]*RPM*.

[[sec-Installing_and_Upgrading]]
=== Installing and Upgrading Packages
indexterm:[RPM,installing]indexterm:[RPM,upgrading]indexterm:[packages,installing RPM]indexterm:[packages,upgrading RPM]indexterm:[RPM,file name]
[application]*RPM* packages typically have file names in the following form:

----
package_name-version-release-operating_system-CPU_architecture.rpm
----

For example the `tree-1.7.0-3.{PKGOS}.x86_64.rpm` file name includes the package name (`tree`), version (`1.7.0`), release (`3`), operating system major version (`{PKGOS}`) and *CPU* architecture (`x86_64`).

.Important
[IMPORTANT]
====

When installing a package, ensure it is compatible with your operating system and processor architecture. This can usually be determined by checking the package name. For example, the file name of an [application]*RPM* package compiled for the AMD64/Intel{nbsp}64 computer architectures ends with `x86_64.rpm`.

====

The [option]`-U` (or [option]`--upgrade`) option has two functions, it can be used to:

* upgrade an existing package on the system to a newer version, or

* install a package if an older version is not already installed.

The [command]#rpm -U _package.rpm_pass:attributes[{blank}]# command is therefore able to either *upgrade* or *install*, depending on the presence of an older version of _package.rpm_ on the system.

Assuming the `tree-1.7.0-3.{PKGOS}.x86_64.rpm` package is in the current directory, log in as `root` and type the following command at a shell prompt to either upgrade or install the [package]*tree* package:

[subs="attributes"]
----
~]#{nbsp}rpm -Uvh tree-1.7.0-3.{PKGOS}.x86_64.rpm
----

[[note-Use_-Uvh_for_nicely-formatted_RPM_installs]]
.Use -Uvh for nicely-formatted RPM installs
[NOTE]
====

The [option]`-v` and [option]`-h` options (which are combined with [option]`-U`) cause [application]*rpm* to print more verbose output and display a progress meter using hash signs.

====

If the upgrade or installation is successful, the following output is displayed:

----
Preparing...                ########################################### [100%]
   1:tree                   ########################################### [100%]
----

[[warning-Always_use_the_-i_install_option_to_install_new_kernel_packages]]
.Always use the -i (install) option to install new kernel packages!
[WARNING]
====

[command]#rpm# provides two different options for installing packages: the aforementioned [option]`-U` option (which historically stands for *upgrade*), and the [option]`-i` option (which historically stands for *install*). Because the [option]`-U` option includes both install and upgrade functions, the use of [command]#rpm -Uvh# with all packages, *except kernel packages*, is recommended.

You should always use the [option]`-i` option to *install* a new kernel package instead of upgrading it. This is because using the [option]`-U` option to upgrade a kernel package removes the previous (older) kernel package, which could render the system unable to boot if there is a problem with the new kernel. Therefore, use the [command]#rpm -i _kernel_package_pass:attributes[{blank}]#  command to install a new kernel *without replacing any older kernel packages*. For more information on installing [package]*kernel* packages, see xref:kernel-module-driver-configuration/Manually_Upgrading_the_Kernel.adoc#ch-Manually_Upgrading_the_Kernel[Manually Upgrading the Kernel].

====

The signature of a package is checked automatically when installing or upgrading a package. The signature confirms that the package was signed by an authorized party. If the verification of the signature fails, an error message is displayed.

If you do not have the appropriate key installed to verify the signature, the message contains the word `NOKEY`:

[subs="attributes"]
----
warning: tree-1.7.0-3.{PKGOS}.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 431d51: NOKEY
----

See xref:RPM.adoc#s1-check-rpm-sig[Checking Package Signatures] for more information on checking package signatures.

[[s3-rpm-errors]]
==== Replacing Already-Installed Packages
indexterm:[RPM,already installed]indexterm:[packages,RPM,already installed]
If a package of the same name and version is already installed, the following output is displayed:

[subs="attributes"]
----
Preparing...                ########################################### [100%]
        package tree-1.7.0-3.{PKGOS}.x86_64 is already installed
----

To install the package anyway, use the [option]`--replacepkgs` option, which tells [application]*RPM* to ignore the error:

[subs="attributes"]
----
~]#{nbsp}rpm -Uvh --replacepkgs tree-1.7.0-3.{PKGOS}.x86_64.rpm
----

This option is helpful if files installed from the package were deleted or if you want the original configuration files to be installed.

If you attempt an upgrade to an *older* version of a package (that is, if a newer version of the package is already installed), [application]*RPM* informs you that a newer version is already installed. To force [application]*RPM* to perform the downgrade, use the [command]#--oldpackage# option:

----
rpm -Uvh --oldpackage older_package.rpm
----

[[s3-rpm-conflicting-files]]
==== Resolving File Conflicts
indexterm:[RPM,file conflicts,resolving]indexterm:[RPM,conflicts]indexterm:[packages,RPM,conflict]
If you attempt to install a package that contains a file that has already been installed by another package, a conflict message is displayed. To make [application]*RPM* ignore this error, use the [command]#--replacefiles# option:

[subs="quotes, macros"]
----
[command]#rpm -Uvh --replacefiles _package.rpm_pass:attributes[{blank}]#
----

[[s3-rpm-unresolved-dependency]]
==== Satisfying Unresolved Dependencies
indexterm:[RPM,dependencies]indexterm:[packages,dependencies]indexterm:[RPM,failed dependencies]indexterm:[packages,RPM,failed dependencies]
[application]*RPM* packages sometimes depend on other packages, which means that they require other packages to be installed to run properly. If you try to install a package that has an unresolved dependency, a message about a failed dependency is displayed.

Find the suggested package(s) on the {MAJOROS} installation media or on one of the active {MAJOROS} mirrors and add it to the installation command. To determine which package contains the required file, use the [option]`--whatprovides` option:

----
rpm -q --whatprovides "required_file"
----

If the package that contains _required_file_ is in the [application]*RPM* database, the name of the package is displayed.

[[warning-Forcing_Package_Installation]]
.Warning
[WARNING]
====

Although you can *force* [command]#rpm# to install a package that has an unresolved dependency (using the [option]`--nodeps` option), this is *not* recommended and will usually result in the installed software failing to run. Installing packages with [option]`--nodeps` can cause applications to misbehave or terminate unexpectedly. It can also cause serious package-management problems or system failure. For these reasons, heed the warnings about missing dependencies. The [application]*DNF* package manager performs automatic dependency resolution and fetches dependencies from on-line repositories.

====

[[sec-Configuration_File_Changes]]
==== Preserving Changes in Configuration Files
indexterm:[RPM,configuration file changes]indexterm:[packages,RPM,configuration file changes]indexterm:[RPM,configuration file changes,conf.rpmsave]
Because [application]*RPM* performs intelligent upgrading of packages with configuration files, you may see the following message:

[subs="macros"]
----
saving pass:quotes[_/etc/configuration_file.conf_] as pass:quotes[_/etc/configuration_file.conf_].rpmsave
----

This message means that the changes you made to the configuration file may not be *forward-compatible* with the new configuration file in the package, so [application]*RPM* saved your original file and installed a new one. You should investigate the differences between the two configuration files and resolve them as soon as possible to ensure that your system continues to function properly.

Alternatively, [application]*RPM* may save the package's *new* configuration file as, for example, `pass:attributes[{blank}]_configuration_file.conf_.rpmnew` and leave the configuration file you modified untouched. You should still resolve any conflicts between your modified configuration file and the new one, usually by merging changes from the old one to the new one, for example using the [command]#diff# program.

[[s2-rpm-uninstalling]]
=== Uninstalling Packages
indexterm:[RPM,uninstalling]indexterm:[packages,removing]indexterm:[packages,RPM,uninstalling]indexterm:[packages,RPM,removing]
Uninstalling a package is just as simple as installing one. Type the following command at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#rpm -e _package_pass:attributes[{blank}]#
----

.rpm -e and package name errors
[NOTE]
====

Note that the command expects only the package *name*, not the name of the original package *file*. If you attempt to uninstall a package using the [command]#rpm{nbsp}-e# command and provide the original full file name, you receive a package-name error.

====

You can encounter dependency errors when uninstalling a package if another installed package depends on the one you are trying to remove. For example:

[subs="attributes"]
----
~]#{nbsp}rpm -e ghostscript
error: Failed dependencies:
        ghostscript is needed by (installed) ghostscript-cups-9.07-16.{PKGOS}.x86_64
        ghostscript is needed by (installed) foomatic-4.0.9-6.{PKGOS}.x86_64
        libgs.so.9()(64bit) is needed by (installed) libspectre-0.2.7-4.{PKGOS}.x86_64
        libijs-0.35.so()(64bit) is needed by (installed) gutenprint-5.2.9-15.{PKGOS}.x86_64
        libijs-0.35.so()(64bit) is needed by (installed) cups-filters-1.0.35-15.{PKGOS}.x86_64
----

[[warning-uninstall-Warning-Forcing_Package_Installation]]
.Warning: Forcing Package Installation
[WARNING]
====

Although you can *force* [command]#rpm# to uninstall a package that has unresolved dependencies (using the [option]`--nodeps` option), this is *not* recommended. Removing packages with [option]`--nodeps` can cause applications from the packages whose dependencies are removed to misbehave or terminate unexpectedly. It can also cause serious package-management problems or system failure. For these reasons, heed the warnings about failed dependencies.

====

[[s2-rpm-freshening]]
=== Freshening Packages
indexterm:[RPM,freshening]indexterm:[packages,RPM,freshening]
Freshening is similar to upgrading, except that only installed packages are upgraded. Type the following command at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#rpm -Fvh _package.rpm_pass:attributes[{blank}]#
----

The [option]`-F` (or [option]`--freshen`) option compares the versions of the packages specified on the command line with the versions of packages that are already installed on the system. When a newer version of an already-installed package is processed by the [option]`--freshen` option, it is upgraded to the newer version. However, the [option]`--freshen` option does not install a package if no previously-installed package of the same name exists. This differs from regular upgrading, as an upgrade installs all specified packages regardless of whether or not older versions of the packages are already installed.

Freshening works for single packages or package groups. For example, freshening can help if you download a large number of different packages, and you only want to upgrade those packages that are already installed on the system. In this case, issue the following command with the `*.rpm` global expression:

[subs="attributes"]
----
~]#{nbsp}rpm -Fvh *.rpm
----

[application]*RPM* then automatically upgrades only those packages that are already installed.

[[s2-rpm-querying]]
=== Querying Packages
indexterm:[RPM,querying]indexterm:[packages,RPM,querying]
The [application]*RPM* database stores information about all [application]*RPM* packages installed on the system. It is stored in the `/var/lib/rpm/` directory and is used for many things, including querying what packages are installed, what version each package is, and for calculating changes to files in packages since their installation. To query this database, use the [command]#rpm# command with the [option]`-q` (or [option]`--query`) option:

----
rpm -q package_name
----

This command displays the package name, version, and release number of the installed package _package_name_. For example:

[subs="attributes"]
----
~]${nbsp}rpm -q tree
tree-1.7.0-3.{PKGOS}.x86_64
----

See the `Package Selection Options` subheading in the *rpm*(8) manual page for a list of options that can be used to further refine or qualify your query. Use options listed below the `Package Query Options` subheading to specify what information to display about the queried packages.

[[s2-rpm-verifying]]
=== Verifying Packages
indexterm:[RPM,verifying]indexterm:[packages,RPM,verifying]
Verifying a package is comparing information about files on the system installed from a package with the same information from the original package. Among other parameters, verifying compares the file size, MD5 sum, permissions, type, owner, and the group of each file.

Use the [command]#rpm# command with the [option]`-V` (or [option]`--verify`) option to verify packages. For example:

[subs="quotes, macros, attributes"]
----
~]${nbsp}pass:attributes[{blank}][command]#rpm -V tree#
----

See the `Package Selection Options` subheading in the *rpm*(8) manual page for a list of options that can be used to further refine or qualify your query. Use options listed below the `Verify Options` subheading to specify what characteristics to verify in the queried packages.

If everything verifies properly, there is no output. If there are any discrepancies, they are displayed. The output consists of lines similar to these:

[subs="attributes"]
----
~]#{nbsp}rpm -V abrt
S.5....T.  c /etc/abrt/abrt.conf
.M.......    /var/spool/abrt-upload
----

The format of the output is a string of nine characters followed by an optional attribute marker and the name of the processed file.

The first nine characters are the results of tests performed on the file. Each test is the comparison of one attribute of the file to the value of that attribute as recorded in the [application]*RPM* database. A single period (`.`) means the test passed, and the question-mark character (`?`) signifies that the test could not be performed. The following table lists symbols that denote specific discrepancies:

[[tab-rpm-verification-symbols]]
.RPM Verification Symbols
indexterm:[RPM,verification]
[options="header"]
|===
|Symbol|Description
|`S`|file size differs
|`M`|mode differs (includes permissions and file type)
|`5`|digest (formerly MD5 sum) differs
|`D`|device major/minor number mismatch
|`L`|*readLink*(2) path mismatch
|`U`|user ownership differs
|`G`|group ownership differs
|`T`|mtime differs
|`P`|capabilities differ
|===

The attribute marker, if present, describes the purpose of the given file. The following table lists the available attribute markers:

[[tab-rpm-verification-markers]]
.RPM Verification Symbols
indexterm:[RPM,verification]
[options="header"]
|===
|Marker|Description
|`c`|configuration file
|`d`|documentation file
|`l`|license file
|`r`|readme file
|===

If you see any output, use your best judgment to determine if you should remove the package, reinstall it, or fix the problem in another way.

[[s1-find-verify-rpm]]
== Finding and Verifying RPM Packages
indexterm:[RPM,finding and verifying RPM packages]
Before using any [application]*RPM* packages, you must know where to find them and be able to verify if you can trust them.

[[s2-rpm-finding]]
=== Finding RPM Packages
indexterm:[packages,finding Fedora RPM packages]indexterm:[RPM,finding Fedora RPM packages]
Although there are many [application]*RPM* repositories on the Internet, for security and compatibility reasons, you should consider installing only official Fedora-provided RPM packages. The following is a list of sources for [application]*RPM* packages:

* indexterm:[{MAJOROS} installation media,installable packages]
  indexterm:[packages,{MAJOROS} installation media]
  Official {MAJOROS} installation media.

* indexterm:[initial RPM repositories,installable packages]
  indexterm:[packages,initial RPM repositories]
  Official [application]*RPM* repositories provided with the [application]*DNF* package manager. See xref:package-management/DNF.adoc#ch-DNF[DNF] for details on how to use the official {MAJOROS} package repositories.

* Unofficial, third-party repositories not affiliated with {OSORG} also provide RPM packages.

.Important
[IMPORTANT]
====

When considering third-party repositories for use with your {MAJOROS} system, pay close attention to the repository's web site with regard to package compatibility before adding the repository as a package source. Alternate package repositories may offer different, incompatible versions of the same software, including packages already included in the {MAJOROS} repositories.

====

[[s1-check-rpm-sig]]
=== Checking Package Signatures
indexterm:[RPM,GnuPG]indexterm:[RPM,checking package signatures]indexterm:[GnuPG,checking RPM package signatures]
[application]*RPM* packages can be signed using [application]*GNU Privacy Guard* (or [application]*GPG*), which helps you make certain that downloaded packages are trustworthy. [application]*GPG* is a tool for secure communication. With [application]*GPG*, you can authenticate the validity of documents and encrypt or decrypt data.

To verify that a package has not been corrupted or tampered with, check its [application]*GPG* signature by using the [command]#rpmkeys# command with the [option]`-K` (or [option]`--checksig`) option:

[subs="quotes, macros"]
----
[command]#rpmkeys -K _package.rpm_pass:attributes[{blank}]#
----

Note that the [application]*DNF* package manager performs automatic checking of [application]*GPG* signatures during installations and upgrades.

[application]*GPG* is installed by default, as well as a set of Red{nbsp}Hat keys for verifying packages. To import additional keys for use with [application]*RPM*, see xref:RPM.adoc#s2-keys-importing[Importing GPG Keys].

[[s2-keys-importing]]
==== Importing GPG Keys

To verify Red Hat packages, a Red{nbsp}Hat [application]*GPG* key needs to be installed. A set of basic keys is installed by default. To view a list of installed keys, execute the following command at a shell prompt:

[subs="attributes"]
----
~]${nbsp}rpm -qa gpg-pubkey*
----

To display details about a specific key, use [command]#rpm{nbsp}-qi# followed by the output from the previous command. For example:

[subs="quotes, macros, attributes"]
----
~]${nbsp}pass:attributes[{blank}][command]#rpm -qi gpg-pubkey-fd431d51-4ae0493b#
----

Use the [command]#rpmkeys# command with the [option]`--import` option to install a new key for use with [application]*RPM*. The default location for storing [application]*RPM* *GPG* keys is the `/etc/pki/rpm-gpg/` directory. To import new keys, use a command like the following as `root`:

[subs="attributes"]
----
~]#{nbsp}rpmkeys --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
----

See the link:++https://access.redhat.com/security/team/key/++[Product Signing (GPG) Keys] article on the Red{nbsp}Hat Customer{nbsp}Portal for additional information about Red{nbsp}Hat package-signing practices.

[[s1-rpm-usage-examples]]
== Common Examples of RPM Usage
indexterm:[RPM,tips]indexterm:[packages,RPM,tips]
[application]*RPM* is a useful tool for both managing your system and diagnosing and fixing problems. See the following examples for an overview of some of the most-used options.

* To verify your entire system and see what files are missing, issue the following command as `root`:
  indexterm:[RPM,finding deleted files with]indexterm:[packages,finding deleted files from]
+
[subs="quotes, macros"]
----
[command]#rpm -Va#
----
+
If some files are missing or appear corrupted, consider reinstalling relevant packages.

* To determine which package owns a file, enter:
  indexterm:[RPM,determining file ownership with]indexterm:[packages,determining file ownership with]
+
[subs="quotes, macros"]
----
[command]#rpm -qf _file_pass:attributes[{blank}]#
----

* To verify the package that owns a particular file, enter as `root`:
+
[subs="quotes, macros"]
----
[command]#rpm -Vf _file_pass:attributes[{blank}]#
----

* To locate documentation files that are a part of a package to which a file belongs, enter:
  indexterm:[RPM,documentation with]indexterm:[packages,locating documentation for]indexterm:[documentation,finding installed]
+
[subs="quotes, macros"]
----
[command]#rpm -qdf _file_pass:attributes[{blank}]#
----

* To find information about a (non-installed) package file, use the following command:
  indexterm:[RPM,querying uninstalled packages]indexterm:[packages,querying uninstalled]
+
[subs="quotes, macros"]
----
[command]#rpm -qip _package.rpm_pass:attributes[{blank}]#
----

* To list files contained in a package, use:
  indexterm:[RPM,querying for file list]indexterm:[packages,obtaining list of files]
+
[subs="quotes, macros"]
----
[command]#rpm -qlp _package.rpm_pass:attributes[{blank}]#
----

See the *rpm*(8) manual page for more options.

[[s1-rpm-additional-resources]]
== Additional Resources
indexterm:[RPM,additional resources]
[application]*RPM* is a complex utility with many options and methods for querying, installing, upgrading, and removing packages. See the following resources to learn more about [application]*RPM*.

.Installed Documentation

 * [command]#rpm --help# — This command displays a quick reference of [application]*RPM* parameters.

 * *rpm*(8) — The [application]*RPM* manual page offers an overview of all available [application]*RPM* parameters.

.Online Documentation

indexterm:[RPM,website]indexterm:[RPM,online documentation]

 * The [application]*RPM* website — link:++https://rpm.org++[]

 * The [application]*RPM* mailing list — link:++http://lists.rpm.org/mailman/listinfo/rpm-list++[]

.See Also

indexterm:[RPM,see also]

 * xref:package-management/DNF.adoc#ch-DNF[DNF] describes how to use the [application]*DNF* package manager to search, install, update, and uninstall packages on the command line.
